﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapNode : ScriptableObject
{
  public enum Type {
    MainStory,
    SideQuest
  }

  public Type NodeType;
  public string Name;
  public string Description;
}
