﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "New Map Node", menuName = "Map Node")]
public class StoryNode : MapNode
{
  public int CashReward;
  public int XPReward;
  public string SceneName;
  public Sprite icon;
}
