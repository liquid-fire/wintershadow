﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "New Spell", menuName = "Spell")]
public class SpellStats : ItemStats
{
  public Sprite SpellIcon;
  public int ManaUsed;
  
  public bool Stuns;
  [Range (0.01f, 2f)]
  public float StunDuration;
  public List<Character.EType> AffectedByStun;
  
  public bool InflictsDamage;
  [Range (5, 40)]
  public int Damage;
  public List<Character.EType> AffectedByDamage;

  public bool Heals;

  public void Attack (List<Character> inAreaOfEffect) {
    if (Stuns) {
      foreach (Character character in inAreaOfEffect) {
        if (AffectedByStun.Contains (character.Type)) {
          // inflict stun to the charater!
          ((Enemy) character).SetStunned (StunDuration);
        }
      }
    }

    if (InflictsDamage) {
      foreach (Character character in inAreaOfEffect) {
        if (AffectedByDamage.Contains (character.Type)) {
          // inflict damage to the charater!
          character.TakeDamage (Damage);
        }
      }
    }
  }
}
