﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemStats : ScriptableObject
{
  public enum ItemType {
    Spell,
    Consumable,
    Armour
  }   

  public ItemType Type;
  [Range (1, 5)] public int Level = 1;
  public string Name;
  public string Description;
}
