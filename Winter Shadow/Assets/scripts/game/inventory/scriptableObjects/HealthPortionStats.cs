﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "New Health Portion", menuName = "Consumable/Health Portion")]
public class HealthPortionStats : ConsumableStats
{
  public int HitPointAdd;
}
