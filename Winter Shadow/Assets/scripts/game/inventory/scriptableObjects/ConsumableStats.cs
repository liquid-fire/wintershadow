﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsumableStats : ItemStats
{
  public enum ConsumableType {
    HealthPortion,
    MamaPortion
  }
  public ConsumableType ConsumableItemType;
}
