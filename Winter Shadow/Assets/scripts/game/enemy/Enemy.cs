﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character
{
  void Start () {
    startupRoutine ();
  }

  public override void TakeDamage(int pointsToTake) {
    base.TakeDamage (pointsToTake);
  }

  protected override void die() {
    Destroy (this.gameObject);
  }

  protected override void startupRoutine () {
    GameManager.Instance.GetComponent<Timer> (GameManager.EPluginReference.Timer).Add (
      regenerateMana,               // call this funtion
      .5f,                          // in very this sec
      "enemy_mana_regen", 
      true
    );
    canMove = true;
    base.startupRoutine ();
  }

  public void SetStunned (float stunDuration) {
    GameManager.Instance.GetComponent<Timer> (GameManager.EPluginReference.Timer).Add (() => {
      canMove = true;
    }, stunDuration);
  }
}
