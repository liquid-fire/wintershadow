﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Player : Character
{
  // some how make this use the player state object
  void Start () {
    startupRoutine ();
  }

  void Update () {
  }

  public override void TakeDamage(int pointsToTake) {
    base.TakeDamage (pointsToTake);
  }

  protected override void die () {
  }

  protected override void startupRoutine () {
    GameManager.Instance.GetComponent<EventBus> (GameManager.EPluginReference.EventBus).RaiseEvent ("PlayerReady", this);
    GameManager.Instance.GetComponent<Timer> (GameManager.EPluginReference.Timer).Add (
      regenerateMana,               // call this funtion
      .1f,                          // in very this sec
      "player_mana_regen", 
      true
    );
    canMove = true;
    base.startupRoutine ();
  }

  public void AddHitPoint (int pointsToAdd) {
    // some health portion or something
    Stats.HitPoint += pointsToAdd;
    if (Stats.HitPoint > 100)
      Stats.HitPoint = 100;
  }
}