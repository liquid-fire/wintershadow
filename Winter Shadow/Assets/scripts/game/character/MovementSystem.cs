﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Character))]
[RequireComponent (typeof (SphereCollider))]
[RequireComponent (typeof (MoveCombatSystem))]
public class MovementSystem : MonoBehaviour
{
  /*
  This class deals with all the movement reallated things in NPCs and Player
   */

  MoveCombatSystem MoveCombatSystem {
    get {
      return transform.GetComponent <MoveCombatSystem> ();
    }
  }
  public InputManager InputManager {
		get {
			return GameManager.Instance.GetComponent<InputManager> (GameManager.EPluginReference.InputManager);
		}
	}

  public enum MovementState {
    Ideal,
    Moving
  }

  [SerializeField] MovementState movementState;

  void OnTriggerEnter(Collider other) {
    if (MoveCombatSystem.IsAIDriven)
    if (other.transform.GetComponent<Player> ()) {
      Logger.Log (this.GetType (), "Player in range");
      MoveCombatSystem.TargetInSpellRange = true;
      MoveCombatSystem.CombatSystem.PlayerInRange ();
    }
  }

  void OnTriggerStay(Collider other) {
    if (MoveCombatSystem.IsAIDriven)
    if (other.transform.GetComponent<Player> ()) {
      MoveCombatSystem.CombatSystem.PlayerInRange ();
    }
  }

  void OnTriggerExit(Collider other) {
    if (MoveCombatSystem.IsAIDriven)
    if (other.transform.GetComponent<Player> ()) {
      Logger.Log (this.GetType (), "Player out of range");
      if (!MoveCombatSystem.CombatSystem.IsAttacking)
        MoveCombatSystem.CurrentTask = MoveCombatSystem.Task.MovingToTarget;
      MoveCombatSystem.TargetInSpellRange = false;
    }
  }
  
  void Update() {
    if (!MoveCombatSystem.IsAIDriven) {
      if (InputManager.CurrentMode == InputManager.InputMode.UserSpellInput)
        return;

      // code for player movement
      if (!MoveCombatSystem.Character.CanMove) {
        movementState = MovementState.Ideal;
        return;
      }
      
      Vector3 moveVector = (-transform.right);
      moveVector.x = InputManager.Horizontal * MoveCombatSystem.CharacterStats.Speed;
      moveVector.z = InputManager.Vertical * MoveCombatSystem.CharacterStats.Speed;
      if (moveVector == Vector3.zero) {
        movementState = MovementState.Ideal;
        return;
      }
      move (moveVector);
      movementState = MovementState.Moving;
    } else {
      // code for enemy movement
      MoveCombatSystem.TargetPosition = GameObject.Find ("Player").transform.position;
      if (MoveCombatSystem.CurrentTask == MoveCombatSystem.Task.MovingToTarget) {
      } else {
        return;
      }
      movementState = MovementState.Moving;
      Vector3 direction = (MoveCombatSystem.TargetPosition - transform.position).normalized;
      Quaternion rotation = Quaternion.LookRotation (direction);
      //transform.rotation = Quaternion.Slerp (transform.rotation, rotation, Time.deltaTime /* * rotaition_speed */);
      Vector3 moveVector = (transform.right);
      if (direction.z < 0)
        direction = -direction;
      moveVector.x = InputManager.Horizontal * MoveCombatSystem.CharacterStats.Speed;
      moveVector.z = InputManager.Vertical * MoveCombatSystem.CharacterStats.Speed;
      moveVector = rotation * direction;
      move (moveVector);
    }
  }

  void move (Vector3 movementVector) {
    transform.Translate (movementVector * Time.deltaTime);
  }
}
