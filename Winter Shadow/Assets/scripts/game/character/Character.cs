﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour
{
    public enum EType {
      Player,
      Water,
      Fire,
      Grass
    }

    public CharacterStats Stats;
    public EType Type;
    protected bool canMove = true;
    public bool CanMove {
      get {
        return canMove;
      }
    }

    protected virtual void startupRoutine () {}
    public virtual void TakeDamage (int pointsToTake) {
      Stats.HitPoint -= pointsToTake;
      if (Stats.HitPoint < 0) { 
        Stats.HitPoint = 0;
        die ();
      }
    }
    protected abstract void die ();
    protected virtual void regenerateMana () {
      if (Stats.ManaPoint < 100)
        Stats.ManaPoint ++;
    }
}
