﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Character))]
[RequireComponent (typeof (MoveCombatSystem))]
public class CombatSystem : MonoBehaviour
{
  [SerializeField] Transform pointOfSpell;
  Transform pointObject;

  MoveCombatSystem MoveCombatSystem {
    get {
      return transform.GetComponent <MoveCombatSystem> ();
    }
  }
  public InputManager InputManager {
		get {
			return GameManager.Instance.GetComponent<InputManager> (GameManager.EPluginReference.InputManager);
		}
	}
  float distanceFromTarget;

  public enum CombatState {
    Waiting,
    CastingSpell,
    Melee
  }
  CombatState combatState;
  public bool IsAttacking {
    get {
      return combatState != CombatState.Waiting;
    }
  }

  void Update () {
    if (!MoveCombatSystem.IsAIDriven) {
      if (InputManager.Fire) {
        meleeAttack ();
      }
    }
  }

  public void PlayerInRange () {
    if (MoveCombatSystem.IsAIDriven) {
      if (MoveCombatSystem.CurrentTask == MoveCombatSystem.Task.AttackingTarget)
        return;
      /* 
      If mana is not more than 50 then the system will default to melee attack
      Else it'll have a choice.
       */
      combatState = CombatState.Waiting;

      Vector3 direction = (MoveCombatSystem.TargetPosition - transform.position).normalized;
      RaycastHit hit;
      if (Physics.Raycast (transform.position, direction, out hit, Mathf.Infinity)) {
        distanceFromTarget = Vector2.Distance (transform.position, hit.transform.position);
        if (distanceFromTarget < 1) {
          MoveCombatSystem.TargetInMeleeRange = true;
        } else {
          MoveCombatSystem.TargetInMeleeRange = false;
        }
      }

      AICombat ();
    }
  }

  void AICombat () {
    int choice = 1;

    // temp mama check code

    if (MoveCombatSystem.CharacterStats.ManaPoint > 50) {
      choice = Random.Range (0,1);
    }

    //
    switch (choice) {
      case 0 : {
        Logger.Log (this.GetType (), "I'll use spell");
        CastSpell (Vector3.one);
        break;
      }

      case 1 : {
        if (!MoveCombatSystem.TargetInMeleeRange) {
          MoveCombatSystem.CurrentTask = MoveCombatSystem.Task.MovingToTarget;
          combatState = CombatState.Waiting;
          Logger.Log (this.GetType (), "I'm not in melee range");
          return;
        }
        Logger.Log (this.GetType (), "I'll use melee");
        meleeAttack ();
        break;
      }
    }
  }

  public void CastSpell (Vector3 position) {
    MoveCombatSystem.CurrentTask = MoveCombatSystem.Task.AttackingTarget;
    combatState = CombatState.CastingSpell;
    Logger.Log (this.GetType (), "Spell casted");
    MoveCombatSystem.CharacterStats.ManaPoint = 0;
    if (pointOfSpell) {
      pointObject = Instantiate (pointOfSpell, position, Quaternion.Euler (90, 0, 0));
    }
    GameManager.Instance.GetComponent <Timer> (GameManager.EPluginReference.Timer).Add (() => {
      MoveCombatSystem.CurrentTask = MoveCombatSystem.Task.MovingToTarget;
      combatState = CombatState.Waiting;
      Destroy (pointObject.gameObject);
    }, 5);
    //MoveCombatSystem.CurrentTask = MoveCombatSystem.Task.MovingToTarget;
  }

  void meleeAttack () {
    MoveCombatSystem.CurrentTask = MoveCombatSystem.Task.AttackingTarget;
    combatState = CombatState.Melee;
    Logger.Log (this.GetType (), "Melee Attack");
    GameManager.Instance.GetComponent <Timer> (GameManager.EPluginReference.Timer).Add (() => {
      MoveCombatSystem.CurrentTask = MoveCombatSystem.Task.MovingToTarget;
      combatState = CombatState.Waiting;
    }, .4f);
  }
}
