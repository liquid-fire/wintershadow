﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "Character Stats", menuName = "Character Stats")]
public class CharacterStats : ScriptableObject
{
  public int HitPoint;
  public int ManaPoint;
  public float Speed;
}
