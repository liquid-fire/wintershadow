﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (MovementSystem))]
[RequireComponent (typeof (CombatSystem))]
public class MoveCombatSystem : MonoBehaviour
{
  /*
  Conditions
  Move as long as target is not in range
  If there is mana left do spell attack else move closer to mele attack
   */

  public Character Character;
  public CharacterStats CharacterStats;
  public Character.EType Type;

  //Sub components
  public MovementSystem MovementSystem;
  public CombatSystem CombatSystem;

  public enum Task {
    MovingToTarget,
    AttackingTarget
  }

  public enum OperationMode {
    AIDriven,
    PlayerDriven
  }

  [SerializeField] OperationMode operationMode = OperationMode.PlayerDriven;

  public Task CurrentTask;
  public delegate void UpdateTask ();
  public UpdateTask TodoNext;
  public Vector3 TargetPosition;

  public InputManager InputManager {
		get {
			return GameManager.Instance.GetComponent<InputManager> (GameManager.EPluginReference.InputManager);
		}
	}
  
  public bool TargetInSpellRange;
  public bool TargetInMeleeRange;

  public bool IsAIDriven  {
    get {
      return operationMode == OperationMode.AIDriven;
    }
  }

  void Start () {
    Character = GetComponent<Character> ();
    CharacterStats = Character.Stats;
    Type = Character.Type;
    MovementSystem = transform.GetComponent<MovementSystem> ();
    CombatSystem = transform.GetComponent<CombatSystem> ();
  }
}
