﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GameManagerPlugin : MonoBehaviour {

	// Custom class over MonoBehaviour to extend its functionallity. Pff. xD 

	public GameManager.EPluginReference Ref;

	public abstract void ResetRoutine ();

	public void SelfDestruct () {
		Destroy (this);
	}

	public virtual void OnLoad () {}
  public virtual void OnUnload () {}
}
