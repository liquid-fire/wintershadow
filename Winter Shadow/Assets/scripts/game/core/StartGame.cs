﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGame : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.Start ();   
        DontDestroyOnLoad (GameObject.Find ("_gameManager")); 
    }
}
