﻿using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class WinterShadow
{
    public Type type;

    abstract public void StartupRoutine ();
}
