﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventBus : GameManagerPlugin {

	public class EventListener {
		public delegate void Callback (object obj);
		public bool IsSingleShot;
		public Callback Method;

		public EventListener () {
			IsSingleShot = true;
		}
	}

	Dictionary <string, IList<EventListener>> m_EventTable;
	Dictionary <string, IList<EventListener>> EventTable {
		get {
			if (m_EventTable == null)
				m_EventTable = new Dictionary<string, IList<EventListener>> ();
			
			return m_EventTable;
		}
	}

	public void ListenTo (string eventName, EventListener listener) {
		if (!EventTable.ContainsKey (eventName))
			EventTable.Add (eventName, new List <EventListener> ());

		if (EventTable[eventName].Contains (listener)) {
			Debug.Log ("Event already added, returning");
			return;
		}

		EventTable[eventName].Add (listener);
	}

	// Object can contain data, which the listener can use
	public void RaiseEvent (string eventName, object obj = null) {
		if (!EventTable.ContainsKey (eventName))
			return;

		Logger.Log ("EventBus", "Raising Event, " + eventName);
		List<EventListener> toRemove = new List<EventListener> ();
		
		for (int i = 0; i < EventTable[eventName].Count; i++) {
			EventListener listener = EventTable[eventName][i];
			listener.Method (obj);
			if (listener.IsSingleShot)
				toRemove.Add (listener);
		}

		foreach (var listener in toRemove) {
			EventTable[eventName].Remove (listener);
		}
	}

  public override void ResetRoutine() {}
}
