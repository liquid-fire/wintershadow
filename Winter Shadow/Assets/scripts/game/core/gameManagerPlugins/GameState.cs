﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class GameState : GameManagerPlugin
{
    enum State
    {
        StartingGame,
        WaitingAtMenu,
        PlayingGame,
        ExitingGame
    }

    [SerializeField] State CurrentGameState;
    [SerializeField] Button playButton;
    
    public override void OnLoad () {
        EventBus.EventListener gameReadyListener = new EventBus.EventListener ();
        gameReadyListener.Method = (object obj) => {
            // some logic to move camera to main menu
            moveCameraToMainMenu ();
            CurrentGameState = State.WaitingAtMenu;

            playButton = GameObject.Find ("UserInterface/Main Menu").GetComponent <MainMenuCanvas> ().PlayGame;
            playButton.onClick.AddListener (() => {
              SetLoading (this.GetType ());
              onPlayButtonClicked ();
            });
        };
        GameManager.Instance.GetComponent<EventBus> (GameManager.EPluginReference.EventBus).ListenTo ("GameLoaded", gameReadyListener);

        // temp
        //onPlayButtonClicked ();
    }

    public void SetLoading (Type type) {
      Logger.Log (this.GetType (), type.Name + " requested loading");
      moveCameraToLoading ();
    }

    void onPlayButtonClicked () {
      CurrentGameState = State.PlayingGame;
      GameManager.Instance.PlugInManager.LoadPlugin<GameProgress> (GameManager.EPluginReference.GameProgress);
      GameManager.Instance.GetComponent<EventBus> (GameManager.EPluginReference.EventBus).RaiseEvent ("StartGamePlay");
    }

    void moveCameraToMainMenu () {
      Logger.Log (this.GetType (), "Loading done, moving camera to Main Menu");
      Vector3 currentPosition = Camera.main.transform.localPosition;
      currentPosition.x = 2616;
      Camera.main.transform.localPosition = currentPosition;
    }

    void moveCameraToLoading () {
      Logger.Log (this.GetType (), "Loading done, moving camera to Main Menu");
      Vector3 currentPosition = Camera.main.transform.localPosition;
      currentPosition.x = 656;
      Camera.main.transform.localPosition = currentPosition;
    }

    public override void ResetRoutine() {}
}
