﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameProgress : GameManagerPlugin
{
  public Player Player;
  public override void OnLoad () {
      EventBus.EventListener startGamePlayListener = new EventBus.EventListener ();
      startGamePlayListener.Method = (object obj) => {
          /* 
            load new scene
            So baisically we need to load the map here. Where the play will select the champain he'd/she'd like to play.

          */ 

          Logger.Log (this.GetType (), "Loading the GamePlay scene"); 
          this.Player = (Player) obj;
          onGameSceneLoaded ();
          //GameManager.Instance.GetComponent <GameState> (GameManager.EPluginReference.GameState).SetLoading (this.GetType ());
          //StartCoroutine (LoadGameScene ());
      };
      GameManager.Instance.GetComponent<EventBus> (GameManager.EPluginReference.EventBus).ListenTo ("PlayerReady", startGamePlayListener);
      //StartCoroutine (LoadGameScene ());
      LoadGameScene ();
  }

  void LoadGameScene () {
    //yield return null;
    // will require the game scene name as all the levels will be different scenes
    Logger.Log (this.GetType (), "Loading the GamePlay scene"); 
    //AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("game");
    SceneManager.LoadScene ("game", LoadSceneMode.Single);
    //onGameSceneLoaded ();

    // while (!asyncLoad.isDone) {
    //   Logger.Log (this.GetType (), "Game scene Loaded");
    //   onGameSceneLoaded ();
    //   yield return null;
    // }
  }
  void onGameSceneLoaded () {
    /*
    Load inventory system here
    Find the spell objects here and add spell components to those objects
    Input system will run thro those objects and listeners
      */

    GameManager.Instance.PlugInManager.LoadPlugin <InputManager> (GameManager.EPluginReference.InputManager);
  }

  IEnumerator LoadMapScene () {
    AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("map");

    while (!asyncLoad.isDone) {
      Logger.Log (this.GetType (), "Map scene Loaded");
      onMapSceneLoaded ();
      yield return null;
    }
  }
  void onMapSceneLoaded () {
    
  }

  public override void OnUnload() {
    GameManager.Instance.PlugInManager.RemovePlugin <InputManager> ();
  }

  public override void ResetRoutine() {}
}
