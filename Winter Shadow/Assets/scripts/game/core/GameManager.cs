﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : WinterShadow
{
	public enum EPluginReference {
		EventBus,
		GameState,
		GameProgress,
    InputManager,
		Timer,
		ScoreKeeper,
		Inventory,
		PlayGame,
		Firebase,
	}

	public override void StartupRoutine() {
		type = this.GetType ();
		Logger.Log (type, "Booting up");
	}

	public GameManager () {
		StartupRoutine ();
	}

  private GameObject gameObject;
	private PluginManager m_PlugInManager;
	/*
		Plugin manager is a component of Game Manager which can extend it functionality
	*/
	public PluginManager PlugInManager {
		get {
			if (m_PlugInManager == null) {
				Logger.Log (type, "Creating Plugin Manager Instance");
				m_PlugInManager = new PluginManager ();
			}
			return m_PlugInManager;
		}
	}
	private static GameManager m_Instance;
    public static GameManager Instance {
		get {
			if (m_Instance == null) {
				m_Instance = new GameManager ();
				Logger.Log (m_Instance.type, "Creating GameObject");
				m_Instance.gameObject = new GameObject("_gameManager");

				// Load Global Plugins
				m_Instance.PlugInManager.LoadPlugin <EventBus> (EPluginReference.EventBus);
				m_Instance.PlugInManager.LoadPlugin <GameState> (EPluginReference.GameState);
        m_Instance.PlugInManager.LoadPlugin <Timer> (EPluginReference.Timer);
				
				// Call when all initial setup done
				m_Instance.GetComponent<EventBus> (EPluginReference.EventBus).RaiseEvent ("GameLoaded");
			}
			return m_Instance;
		}
	}

	public Dictionary<EPluginReference, GameManagerPlugin> PlugInData {
		get {
			return m_PlugInManager.activePlugIn;
		}
	}

	public T GetComponent<T> (EPluginReference reference) where T : GameManagerPlugin {
		return (T) m_PlugInManager.GetComponent (reference);
	}

	public void RunResetRoutine () {
		foreach (GameManagerPlugin plugin in m_PlugInManager.activePlugIn.Values) {
			plugin.ResetRoutine ();
		}
	}

	public void Start () {}

	public class PluginManager {
		public Dictionary<EPluginReference, GameManagerPlugin> activePlugIn;

		public PluginManager () {
			activePlugIn = new Dictionary<EPluginReference, GameManagerPlugin> ();
		}

		public GameManagerPlugin GetComponent (EPluginReference reference) {
			if (activePlugIn.ContainsKey (reference)) {
				return activePlugIn [reference];
			}
			return null;
		}

		public void LoadPlugin<T> (EPluginReference reference) where T : GameManagerPlugin {
      if (activePlugIn.ContainsKey (reference))
        return;
			T t = m_Instance.gameObject.AddComponent<T> ();
			Logger.Log ("PluginManager", "Loading new plugin, " + t.GetType ().Name);
			t.OnLoad ();
			t.Ref = reference;
			activePlugIn.Add (reference, t);
		}

		public void RemovePlugin<T> () where T : GameManagerPlugin {
			T t = m_Instance.gameObject.GetComponent<T> ();
      t.OnUnload ();
			Logger.Log ("PluginManager", "Unloading plugin, " + t.GetType ().Name);
			activePlugIn.Remove (t.Ref);
			if (t != null)
				t.SelfDestruct ();
		}
	}
}