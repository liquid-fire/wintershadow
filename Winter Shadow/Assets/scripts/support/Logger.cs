﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Logger
{
    public static void Log (Type type, String log) {
        Debug.Log (type.Name + ":" + DateTime.Now + ": " + log);
    }

    public static void Log (String type, String log) {
        Debug.Log (type + ":" + DateTime.Now + ": " + log);
    }
}
